import java.time.LocalDate;
import java.time.LocalDateTime;


public class Eintrag {
	
	public EintragTyp t;
	public LocalDateTime timedate;
	
	public String quelle;
	public int id;
	public String beschreibung = "";
	public String KOPIEtypeKOPIE ="";
	
	
	public enum EintragTyp
	{
		INFORMATION, FEHLER, KRITISCH, WARNING, UNBEKANNT; 
	}
	
	public void setType(String s)
	{
		KOPIEtypeKOPIE = s.toLowerCase().toString();
		
		switch (s.toLowerCase().toString()) {
		case "informationen": t = EintragTyp.INFORMATION; break;
		case "fehler": t = EintragTyp.FEHLER; break;
		case "warnung": t = EintragTyp.WARNING; break;
		case "kritisch": t = EintragTyp.KRITISCH; break;
		default: t = EintragTyp.UNBEKANNT;
			break;
		}
	}


	@Override
	public String toString() {
		return "ID: "+id+", "+t + " - "+getDateRight() + " - "+quelle + " " + maxBeschr() ;
	}

	private String maxBeschr() {
		if(beschreibung.length() > 150)
			return beschreibung.substring(0, 150).trim()+ "...";
		return beschreibung;
	}


	private String getDateRight() {
		
		String month = timedate.getMonthValue()+"";
		String day = timedate.getDayOfMonth()+"";
		
		if(month.length() == 1)
			month = "0"+month;
		if(day.length() == 1)
			day = "0"+day;
		
		return timedate.getYear()+""+month+""+day+"";
	}
	
	

}
